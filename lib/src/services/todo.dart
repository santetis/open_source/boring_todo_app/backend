import 'dart:io';

import 'package:backend/src/get_user_with_authorization.dart';
import 'package:database_interface/database_interface.dart';
import 'package:generated_protos/generated_protos.dart';
import 'package:grpc/grpc.dart';

class TodoService extends TodoServiceBase {
  NoSqlDatabasePool pool;

  TodoService(this.pool);

  @override
  Future<Todo> createTodo(ServiceCall call, CreateTodo request) async {
    final authorization = call.clientMetadata[HttpHeaders.authorizationHeader];
    if (authorization == null) {
      throw GrpcError.unauthenticated();
    }
    final connection = await pool.pool.get();
    final db = connection.connection;
    final user = await getUserWithAuthorization(db, authorization);
    if (user == null) {
      await connection.release();
      throw GrpcError.notFound();
    }
    final todos = db.collection('todos');
    await todos.insert({
      'users': [user.id],
      'title': request.title,
      'description': request.description,
      'checked': false,
    });
    final todo = await todos.find().map((todo) {
      final t = Todo()
        ..id = (todo['_id'] as DatabaseId).id
        ..title = todo['title'];
      t.users
        ..clear()
        ..addAll(todo['users']);
      return t;
    }).firstWhere((todo) =>
        todo.users.contains(user.id) &&
        todo.title == request.title &&
        todo.description == request.description &&
        todo.checked == false);
    await connection.release();
    return todo;
  }

  @override
  Future<Empty> deleteTodo(ServiceCall call, CreateTodo request) {
    throw GrpcError.unimplemented();
  }

  @override
  Future<Empty> updateTodo(ServiceCall call, CreateTodo request) {
    throw GrpcError.unimplemented();
  }
}
